/*========================================================================*\
|*  This file is part of mdl2unr.                                         *|
|*                                                                        *|
|*  mdl2unr is free software: you can redistribute it and/or modify       *|
|*  it under the terms of the GNU General Public License as published by  *|
|*  the Free Software Foundation, either version 3 of the License, or     *|
|*  (at your option) any later version.                                   *|
|*                                                                        *|
|*  mdl2unr is distributed in the hope that it will be useful,             *|
|*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *|
|*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *|
|*  GNU General Public License for more details.                          *|
|*                                                                        *|
|*  You should have received a copy of the GNU General Public License     *|
|*  along with mdl2unr.  If not, see <http://www.gnu.org/licenses/>.      *|
|*                                                                        *|
\*========================================================================*/

/*========================================================================
 * md2.c : routines for converting idTech 2 meshes to Unreal Engine 1
 * 
 * written by Adam 'Xaleros' Smith
 *========================================================================
*/

#include "md2.h"
#include "u3d.h"
#include "mem.h"
#include "error.h"
#include "uscript.h"
#include <string.h>
#include <ctype.h>

char *MD2_GetAnimGroup (const char *frame_name)
{
	char *group, *ptr;
	
	group = U_Malloc (strlen (frame_name) + 1);
	ptr = group;
	
	while (*frame_name && !isdigit (*frame_name))
		*ptr++ = *frame_name++;
	
	*ptr = '\0';
	return group;
}

int MD2_Load (const char *filename, struct md2_model *mdl)
{
	FILE *fp;
	int i;

	fp = fopen (filename, "rb");
	if (!fp)
	{
		printf ("Error: couldn't open '%s'!\n", filename);
		exit (ERR_FILEMIS);
	}

	fread (&mdl->header, 1, sizeof (struct md2_header), fp);
	if ((mdl->header.ident != 844121161) || (mdl->header.version != 8))
	{
		printf ("Error: bad version or identifier for file '%s'\n", filename);
		fclose (fp);
		exit (ERR_BADIMP);
	}

	mdl->skins     = U_Malloc (sizeof (struct md2_skin) * mdl->header.num_skins);
	mdl->texcoords = U_Malloc (sizeof (struct md2_texCoord) * mdl->header.num_st);
	mdl->triangles = U_Malloc (sizeof (struct md2_triangle) * mdl->header.num_tris);
	mdl->frames    = U_Malloc (sizeof (struct md2_frame) * mdl->header.num_frames);

	/* read md2 mesh data */
  fseek (fp, mdl->header.offset_skins, SEEK_SET);
  fread (mdl->skins, sizeof (struct md2_skin), mdl->header.num_skins, fp);

  fseek (fp, mdl->header.offset_st, SEEK_SET);
  fread (mdl->texcoords, sizeof (struct md2_texCoord), mdl->header.num_st, fp);

  fseek (fp, mdl->header.offset_tris, SEEK_SET);
  fread (mdl->triangles, sizeof (struct md2_triangle), mdl->header.num_tris, fp);

  /* read frames */
  fseek (fp, mdl->header.offset_frames, SEEK_SET);
  for (i = 0; i < mdl->header.num_frames; ++i)
  {
    /* Memory allocation for vertices of this frame */
    mdl->frames[i].verts = U_Malloc (sizeof (struct md2_vertex) * mdl->header.num_vertices);

    /* Read frame data */
    fread (mdl->frames[i].scale, sizeof (vec3_t), 1, fp);
    fread (mdl->frames[i].translate, sizeof (vec3_t), 1, fp);
    fread (mdl->frames[i].name, sizeof (char), 16, fp);
    fread (mdl->frames[i].verts, sizeof (struct md2_vertex), mdl->header.num_vertices, fp);
  }

  fclose (fp);
  return 1;
}

int MD2_To_U3D (struct md2_model *md2)
{
	int i,j;
	char *data_filepath;
	char *aniv_filepath;
	char *uc_filepath;
	const char *mesh_name;
	char *aniv_name;
	char *data_name;
	char *last_frame_group;
	FILE *u3d_data;
	FILE *u3d_aniv;
	FILE *u3d_script;
	u32 aniv_vertex;
	u32 startframe;
	
	struct md2_frame    *frame;
	struct md2_vertex   *vtx;
	struct u3d_aniv_hdr *aniv_hdr;
	struct u3d_data_hdr *data_hdr;
	struct u3d_triangle *triangle;

	vec3_t v;
	
	aniv_hdr = U_Malloc (sizeof (struct u3d_aniv_hdr));
	data_hdr = U_Malloc (sizeof (struct u3d_data_hdr));
	triangle = U_Malloc (sizeof (struct u3d_triangle));
	data_filepath = U3D_GetDataFilePath ();
	aniv_filepath = U3D_GetAnivFilePath ();
	uc_filepath   = U3D_GetScriptFilePath ();
	mesh_name     = U3D_GetMeshMapName ();
	u3d_data = fopen (data_filepath, "wb");
	u3d_aniv = fopen (aniv_filepath, "wb");
	aniv_vertex = 0;
	startframe = 0;

	if (u3d_data == NULL)
	{
		printf ("Cannot create data file '%s'!\n", data_filepath);
		
		U_Free (uc_filepath);
		U_Free (aniv_filepath);
		U_Free (data_filepath);
		U_Free (triangle);
		U_Free (data_hdr);
		U_Free (aniv_hdr);
		
		exit (ERR_NOWRITE);
	}
	if (u3d_aniv == NULL)
	{
		printf ("Cannot create aniv file '%s'!\n", aniv_filepath);
		
		fclose (u3d_data);
		U_Free (uc_filepath);
		U_Free (aniv_filepath);
		U_Free (data_filepath);
		U_Free (triangle);
		U_Free (data_hdr);
		U_Free (aniv_hdr);
		
		exit (ERR_NOWRITE);
	}
	
	/* don't need filepaths anymore */
	U_Free (aniv_filepath);
	U_Free (data_filepath);

	/* write data header */
	data_hdr->num_polygons = md2->header.num_tris;
	data_hdr->num_vertices = md2->header.num_vertices;
	fwrite (data_hdr, sizeof (struct u3d_data_hdr), 1, u3d_data);
	U_Free (data_hdr);

	/* write poly data */
	for (i = 0; i < md2->header.num_tris; i++)
	{
		for (j = 0; j < 3; j++)
		{
			triangle->vertex[j] = md2->triangles[i].vertex[j];
			triangle->tex[j][0] = ((float)md2->texcoords[md2->triangles[i].st[j]].s / md2->header.skinwidth) * 255;
			triangle->tex[j][1] = ((float)md2->texcoords[md2->triangles[i].st[j]].t / md2->header.skinheight) * 255;
		}

		triangle->type = 0; /*?*/
		triangle->color = 255; /*?*/
		triangle->texture_num = 0;
		triangle->flags = 0;
		fwrite (triangle, 1, sizeof (struct u3d_triangle), u3d_data);
	}

	/* clean up data file stuff */
	U_Free (triangle);
	fflush (u3d_data);
	fclose (u3d_data);

	/* write aniv header */
	aniv_hdr->num_frames = md2->header.num_frames;
	aniv_hdr->frame_size = 4 * md2->header.num_vertices;
	fwrite (aniv_hdr, sizeof (struct u3d_aniv_hdr), 1, u3d_aniv);
	U_Free (aniv_hdr);
	
	/* open uc file */
	u3d_script = fopen (uc_filepath, "w");
	
	if (u3d_script == NULL)
	{
		printf ("Cannot create script file '%s'!\n", uc_filepath);
		U_Free (uc_filepath);
		exit (ERR_NOWRITE);
	}
	
	U_Free (uc_filepath);
	
	/* write basic mesh statements */
	aniv_name = U3D_GetAnivFileName ();
	data_name = U3D_GetDataFileName ();
	
	fprintf (u3d_script, CLASS_DECL_STR, mesh_name);
	fprintf (u3d_script, MESH_IMPORT_STR, mesh_name, aniv_name, data_name);
	fprintf (u3d_script, MESH_ORIGIN_STR, mesh_name);
	fprintf (u3d_script, MESH_SEQ_STR, mesh_name, "All", 0, md2->header.num_frames);
	
	U_Free (aniv_name);
	U_Free (data_name);

	/* write aniv data */
	for (i = 0; i < md2->header.num_frames; i++)
	{
		if (frame)
			last_frame_group = MD2_GetAnimGroup (frame->name);
			
		frame = &md2->frames[i];
		
		for (j = 0; j < md2->header.num_vertices; j++)
		{
			vtx = &frame->verts[j];
			for (int k = 0; k < 3; k++)
			{
				v[k] = (vtx->v[k] * frame->scale[k]) + frame->translate[k];
			}
			aniv_vertex = ((int)(v[0] * 8.0) & 0x7FF) | (((int)(v[1] * 8.0) & 0x7FF) << 11 ) | (((int)(v[2] * 4.0) & 0x3FF) << 22);
			fwrite (&aniv_vertex, 4, 1, u3d_aniv);
		}
		
		/* check if current frame name contains the name of the last animation frame */
		if (strstr (frame->name, last_frame_group) == NULL)
		{
			fprintf (u3d_script, MESH_SEQ_STR, mesh_name, last_frame_group, startframe, i - startframe);
			startframe = i;
		}
		else if (md2->header.num_frames == (i+1))
		{
		  fprintf (u3d_script, MESH_SEQ_STR, mesh_name, last_frame_group, startframe, i - startframe + 1);
		}
		
		U_Free (last_frame_group);
	}
	
	/* write meshmap scale and texture import execs */
	fprintf (u3d_script, TEX_IMPORT_STR, mesh_name, mesh_name); 
	fprintf (u3d_script, MESH_SCALE_STR, mesh_name);
	fprintf (u3d_script, MESH_SETTEX_STR, mesh_name, 0, mesh_name);

	/* final cleanup */
	fclose (u3d_script);
	fclose (u3d_aniv);
	
	return 1;
}

void MD2_Free (struct md2_model *mdl)
{
	U_Free (mdl->skins);
	U_Free (mdl->texcoords);
	U_Free (mdl->triangles);
	U_Free (mdl->frames->verts);
	U_Free (mdl->frames);
	U_Free (mdl);
	return;
}
