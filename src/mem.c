/*========================================================================*\
|*  This file is part of mdl2unr.                                         *|
|*                                                                        *|
|*  mdl2unr is free software: you can redistribute it and/or modify       *|
|*  it under the terms of the GNU General Public License as published by  *|
|*  the Free Software Foundation, either version 3 of the License, or     *|
|*  (at your option) any later version.                                   *|
|*                                                                        *|
|*  mdl2unr is distributed in the hope that it will be useful,             *|
|*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *|
|*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *|
|*  GNU General Public License for more details.                          *|
|*                                                                        *|
|*  You should have received a copy of the GNU General Public License     *|
|*  along with mdl2unr.  If not, see <http://www.gnu.org/licenses/>.      *|
|*                                                                        *|
\*========================================================================*/

/*========================================================================
 * mem.c : memory allocation helper functions
 * 
 * written by Adam 'Xaleros' Smith
 *========================================================================
*/

#include "types.h"
#include "mem.h"
#include "error.h"

void *U_Malloc (size_t size)
{
	void *r = _m2u_malloc (size);
	if (r == NULL)
	{
		printf ("Out of memory for size '%" PRSizeT "'! Aborting!\n", size);
		exit (ERR_NOMEM);
	}
	return r;
}

void *U_Realloc (void *ptr, size_t size)
{
	void *r = _m2u_realloc (ptr, size);
	if (r == NULL)
	{
		printf ("Out of memory for size '%" PRSizeT "'! Aborting!\n", size);
		exit (ERR_NOMEM);
	}
	return r;
}
