/*========================================================================*\
|*  This file is part of mdl2unr.                                         *|
|*                                                                        *|
|*  mdl2unr is free software: you can redistribute it and/or modify       *|
|*  it under the terms of the GNU General Public License as published by  *|
|*  the Free Software Foundation, either version 3 of the License, or     *|
|*  (at your option) any later version.                                   *|
|*                                                                        *|
|*  mdl2unr is distributed in the hope that it will be useful,             *|
|*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *|
|*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *|
|*  GNU General Public License for more details.                          *|
|*                                                                        *|
|*  You should have received a copy of the GNU General Public License     *|
|*  along with mdl2unr.  If not, see <http://www.gnu.org/licenses/>.      *|
|*                                                                        *|
\*========================================================================*/

/*========================================================================
 * state.c : routines useful for converting any mesh type and maintaining
 *           the state of the conversion
 * 
 * written by Adam 'Xaleros' Smith
 *========================================================================
*/

#include "u3d.h"
#include "mem.h"
#include "state.h"
#include <string.h>

char *U3D_GetDataFilePath ()
{
	char *out = U_Malloc (strlen (u3d_filename)+6);
	strcpy (out, u3d_filename);
	strcat (out, "_d.3d");
	return out;
}

char *U3D_GetAnivFilePath ()
{
	char *out = U_Malloc (strlen (u3d_filename)+6);
	strcpy (out, u3d_filename);
	strcat (out, "_a.3d");
	return out;
}

char *U3D_GetDataFileName ()
{
	char *path;
	char *name;
	
	path = U3D_GetDataFilePath ();
	name = strrchr (path, '/');
	return (name) ? strdup(++name) : path;
}

char *U3D_GetAnivFileName ()
{
	char *path;
	char *name;
	
	path = U3D_GetAnivFilePath ();
	name = strrchr (path, '/');
	return (name) ? strdup(++name) : path;
}

char *U3D_GetScriptFilePath ()
{
	char *out = U_Malloc (strlen (u3d_filename)+4);
	strcpy (out, u3d_filename);
	strcat (out, ".uc");
	return out;
}

const char *U3D_GetMeshMapName ()
{
	const char *out = strrchr (u3d_filename, '/');
	return (out) ? ++out : u3d_filename;
}

int Sys_GetFormatFromFilename (const char *filename)
{
	if (strstr (filename, ".md2"))
		return IMP_MD2;
  else if (strstr (filename, ".md3"))
    return IMP_MD3;
	else
		return IMP_UNK;
}

void Sys_ReplaceChar (char *str, char old_char, char new_char)
{
	while ( (str = strchr (str, old_char)) != NULL )
		*str = new_char;
}
