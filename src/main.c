/*========================================================================*\
|*  This file is part of mdl2unr.                                         *|
|*                                                                        *|
|*  mdl2unr is free software: you can redistribute it and/or modify       *|
|*  it under the terms of the GNU General Public License as published by  *|
|*  the Free Software Foundation, either version 3 of the License, or     *|
|*  (at your option) any later version.                                   *|
|*                                                                        *|
|*  mdl2unr is distributed in the hope that it will be useful,             *|
|*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *|
|*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *|
|*  GNU General Public License for more details.                          *|
|*                                                                        *|
|*  You should have received a copy of the GNU General Public License     *|
|*  along with mdl2unr.  If not, see <http://www.gnu.org/licenses/>.      *|
|*                                                                        *|
\*========================================================================*/

/*========================================================================
 * main.c : entry point for mdl2unr
 * 
 * written by Adam 'Xaleros' Smith
 *========================================================================
*/

#include "mem.h"
#include "md2.h"
#include "md3.h"
#include "state.h"
#include "error.h"
#include <string.h>

char *mdl_filename = NULL;
char *u3d_filename = NULL;

void PrintHelpAndExit (int code)
{
	printf ("Usage: mdl2unr [OPTIONS] [FILE_IN] [MESH_NAME]\n");
	printf ("Converts one model format to an Unreal Engine 1 format\n\n");
		
	printf ("Arguments:\n");
	printf ("  -i [FORMAT]    Specified import format\n\n");
	
	printf ("Supported import formats:\n");
	//printf ("  idTech 1 Mesh (.mdl)\n");
	printf ("  idTech 2 Mesh (.md2)\n");
	printf ("  idTech 3 Mesh (.md3)\n\n");
		
	printf ("Supported export formats:\n");
	printf ("  Unreal Vertex Mesh  (_*.3d)\n\n");
	//printf ("  Unreal Skeletal Mesh (*.psk | *.psa)\n\n");
	
	printf ("Exit status:\n");
	printf ("  %i  if OK,\n", ERR_OK);
	printf ("  %i  if unknown or bad argument,\n", ERR_UNKARG);
	printf ("  %i  if import model not found,\n", ERR_FILEMIS);
	printf ("  %i  bad import header,\n", ERR_BADIMP);
	printf ("  %i  unknown import format specified,\n", ERR_UNKIMP);
	printf ("  %i  no write permissions,\n", ERR_NOWRITE);
	printf ("  %i  unexpected error,\n", ERR_UNKNOWN);
	
	exit (code);
}

int GetImportFormat (const char *type)
{
	if (strncmp (type, "mdl", 3))
		return IMP_MDL;
	else if (strncmp (type, "md2", 3))
		return IMP_MD2;
  else if (strncmp (type, "md3", 3))
		return IMP_MD3;
	/*else if (strncmp (type, "md5", 3))
		return IMP_MD5;*/
	else
		return IMP_UNK;
}

int main (int argc, char **argv)
{
	int i;
	void *mdl_buffer;
	
	if (argc <= 1)
		PrintHelpAndExit (ERR_UNKARG);
	
	i = 1;
	while (i < argc)
	{
		if (argv[i][0] == '-')
		{
			if (argv[i][1] == 'i')
			{
				if (++i > argc)
					PrintHelpAndExit (ERR_UNKARG);
					
				import_format = GetImportFormat (argv[i++]);
			}
		}
		
		else
		{
			if (mdl_filename == NULL)
				mdl_filename = argv[i++];
				
			else if (u3d_filename == NULL)
				u3d_filename = argv[i++];
				
			else
				PrintHelpAndExit (ERR_UNKARG);
		}
	}
	
	if (mdl_filename == NULL || u3d_filename == NULL)
		PrintHelpAndExit (ERR_UNKARG);
		
	/* change back slashes to forward slashes */
	Sys_ReplaceChar (mdl_filename, '\\', '/');
	Sys_ReplaceChar (u3d_filename, '\\', '/');
	
	if (import_format == IMP_UNK)
	{
		import_format = Sys_GetFormatFromFilename (mdl_filename);
		
		if (import_format == IMP_UNK)
		{
			printf ("Error: unknown import mesh format.\n");
			exit (ERR_UNKIMP);
		}
	}
		
	switch (import_format)
	{
	case IMP_MD2:
		mdl_buffer = U_Malloc (sizeof (struct md2_model));
		MD2_Load (mdl_filename, mdl_buffer);
		MD2_To_U3D (mdl_buffer);
		MD2_Free (mdl_buffer);
		break;
	case IMP_MD3:
		mdl_buffer = U_Malloc (sizeof (struct md3_model));
		MD3_Load (mdl_filename, mdl_buffer);
		MD3_To_U3D (mdl_buffer);
		MD3_Free (mdl_buffer);
		break;
	}
	
	return ERR_OK;
}
