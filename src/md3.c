/*========================================================================*\
|*  This file is part of mdl2unr.                                         *|
|*                                                                        *|
|*  mdl2unr is free software: you can redistribute it and/or modify       *|
|*  it under the terms of the GNU General Public License as published by  *|
|*  the Free Software Foundation, either version 3 of the License, or     *|
|*  (at your option) any later version.                                   *|
|*                                                                        *|
|*  mdl2unr is distributed in the hope that it will be useful,             *|
|*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *|
|*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *|
|*  GNU General Public License for more details.                          *|
|*                                                                        *|
|*  You should have received a copy of the GNU General Public License     *|
|*  along with mdl2unr.  If not, see <http://www.gnu.org/licenses/>.      *|
|*                                                                        *|
\*========================================================================*/

/*========================================================================
 * md3.c : routines for converting idTech 3 meshes to Unreal Engine 1
 * 
 * written by Adam 'Xaleros' Smith
 *========================================================================
*/

#include "md3.h"
#include "u3d.h"
#include "mem.h"
#include "error.h"
#include "uscript.h"
#include "state.h"
#include <string.h>
#include <ctype.h>

#define MD3_XYZ_SCALE (1.0/64)

char *MD3_GetAnimGroup (const char *frame_name)
{
	char *group, *ptr;
	
	group = U_Malloc (strlen (frame_name) + 1);
	ptr = group;
	
	while (*frame_name && !isdigit (*frame_name))
		*ptr++ = *frame_name++;
	
	*ptr = '\0';
	return group;
}

int MD3_Load (const char *filename, struct md3_model *mdl)
{
	FILE *fp;
	int i,k;
	size_t surf_offset;
	struct md3_surface *surf;
	
	fp = fopen (filename, "rb");
	if (!fp)
	{
		printf ("Error: couldn't open '%s'!\n", filename);
		exit (ERR_FILEMIS);
	}
	
	fread (&mdl->header, 1, sizeof (struct md3_header), fp);
	if ((mdl->header.ident != MD3_IDENT))
	{
		printf ("Error: bad version or identifier for file '%s'\n", filename);
		fclose (fp);
		U_Free (mdl);
		exit (ERR_BADIMP);
	}
	
	/* allocate md3 mesh arrays */
	mdl->frames = U_Malloc (sizeof (struct md3_frame) * mdl->header.num_frames);
	mdl->tags   = U_Malloc (sizeof (struct md3_tag)   * mdl->header.num_tags);
	mdl->surfs  = U_Malloc (sizeof (struct md3_surface) * mdl->header.num_surfaces);
	
	/* read md3 frame data */
	fseek (fp, mdl->header.offset_frames, SEEK_SET);
	for (i = 0; i < mdl->header.num_frames; i++)
		fread (&mdl->frames[i], 1, sizeof (struct md3_frame), fp);
	
	/* read md3 tag data */
	fseek (fp, mdl->header.offset_tags, SEEK_SET);
	for (i = 0; i < mdl->header.num_tags; i++)
		fread (&mdl->tags[i], 1, sizeof (struct md3_tag), fp);
		
	/* read md3 surfaces */
	fseek (fp, mdl->header.offset_surfaces, SEEK_SET);
	for (i = 0; i < mdl->header.num_surfaces; i++)
	{
		surf = &mdl->surfs[i];
		surf_offset = ftell (fp);
		
		fread (&surf->info, 1, sizeof (struct md3_surf_info), fp);
		
		if ((surf->info.ident != MD3_IDENT))
		{
			printf ("Error: bad MD3 surface identifier for file '%s'\n", filename);
			U_Free (mdl->surfs);
			U_Free (mdl->tags);
			U_Free (mdl->frames);
			U_Free (mdl);
			exit (ERR_BADIMP);
		}
		
		/* allocate surface arrays */
		surf->shaders   = U_Malloc (sizeof (struct md3_shader)   * surf->info.num_shaders);
		surf->triangles = U_Malloc (sizeof (struct md3_triangle) * surf->info.num_tris);
		surf->texcoords = U_Malloc (sizeof (struct md3_texCoord) * surf->info.num_verts);
		surf->xyz_norms = U_Malloc (sizeof (struct md3_vertex)   * surf->info.num_verts);
		
		/* surface arrays are usually next to each other,
		 * but what if there's an instance where it's not?
		 * it's easier to assume they're in order though... */
		 
		/* read md3 surface shaders */
		fseek (fp, surf_offset + surf->info.offset_shaders, SEEK_SET);
		for (k = 0; k < surf->info.num_shaders; k++)
			fread (&surf->shaders[k], 1, sizeof (struct md3_shader), fp);
			
		/* read md3 surface triangles */
		fseek (fp, surf_offset + surf->info.offset_tris, SEEK_SET);
		for (k = 0; k < surf->info.num_tris; k++)
			fread (&surf->triangles[k], 1, sizeof (struct md3_triangle), fp);
			
		/* read md3 uv coordinates */
		fseek (fp, surf_offset + surf->info.offset_st, SEEK_SET);
		for (k = 0; k < surf->info.num_verts; k++)
			fread (&surf->texcoords[k], 1, sizeof (struct md3_texCoord), fp);
		
		/* read md3 vertices */
		fseek (fp, surf_offset + surf->info.offset_xyznorm, SEEK_SET);
		for (k = 0; k < (surf->info.num_verts * surf->info.num_frames); k++)
			fread (&surf->xyz_norms[k], 1, sizeof (struct md3_vertex), fp);
			
		/* seek to end of surface */
		fseek (fp, surf_offset + surf->info.offset_end, SEEK_SET);
	}
	
	fclose (fp);
	return 1;
}

int MD3_To_U3D (struct md3_model *md3)
{
	int i,j,k;
	char *data_filepath;
	char *aniv_filepath;
	char *uc_filepath;
	const char *mesh_name;
	char *aniv_name;
	char *data_name;
	char *last_frame_group;
	FILE *u3d_data;
	FILE *u3d_aniv;
	FILE *u3d_script;
	u32 aniv_vertex;
	u32 start_frame;
	u32 index_offset;
	
	struct md3_frame    *frame;
	struct md3_vertex   *vtx;
	struct md3_surface  *surf;
	struct u3d_aniv_hdr *aniv_hdr;
	struct u3d_data_hdr *data_hdr;
	struct u3d_triangle *triangle;
	
	vec3_t v;
	
	aniv_hdr = U_Malloc (sizeof (struct u3d_aniv_hdr));
	data_hdr = U_Malloc (sizeof (struct u3d_data_hdr));
	triangle = U_Malloc (sizeof (struct u3d_triangle));
	data_filepath = U3D_GetDataFilePath ();
	aniv_filepath = U3D_GetAnivFilePath ();
	uc_filepath   = U3D_GetScriptFilePath ();
	mesh_name     = U3D_GetMeshMapName ();
	u3d_data = fopen (data_filepath, "wb");
	u3d_aniv = fopen (aniv_filepath, "wb");
	aniv_vertex = 0;
	start_frame = 0;

	if (u3d_data == NULL)
	{
		printf ("Cannot create data file '%s'!\n", data_filepath);
		
		U_Free (uc_filepath);
		U_Free (aniv_filepath);
		U_Free (data_filepath);
		U_Free (triangle);
		U_Free (data_hdr);
		U_Free (aniv_hdr);
		
		exit (ERR_NOWRITE);
	}
	if (u3d_aniv == NULL)
	{
		printf ("Cannot create aniv file '%s'!\n", aniv_filepath);
		
		fclose (u3d_data);
		U_Free (uc_filepath);
		U_Free (aniv_filepath);
		U_Free (data_filepath);
		U_Free (triangle);
		U_Free (data_hdr);
		U_Free (aniv_hdr);
		
		exit (ERR_NOWRITE);
	}
	
	/* don't need filepaths anymore */
	U_Free (aniv_filepath);
	U_Free (data_filepath);

	/* write data header */
	data_hdr->num_polygons = 0;
	data_hdr->num_vertices = 0;
	
	for (i = 1; i < 2/*md3->header.num_surfaces*/; i++)
	{
		data_hdr->num_polygons += md3->surfs[i].info.num_tris;	
		data_hdr->num_vertices += md3->surfs[i].info.num_verts;
	}
	
	fwrite (data_hdr, sizeof (struct u3d_data_hdr), 1, u3d_data);
	U_Free (data_hdr);

	/* write poly data */
	index_offset = 0;
	for (i = 1; i < 2/*md3->header.num_surfaces*/; i++)
	{
		surf = &md3->surfs[i];
		for (j = 0; j < surf->info.num_tris; j++)
		{
			for (k = 0; k < 3; k++)
			{
				triangle->vertex[k] = (u16)surf->triangles[j].indices[k] + index_offset;
				triangle->tex[k][0] = (u8)(surf->texcoords[surf->triangles[j].indices[k]].s * 256);
				triangle->tex[k][1] = (u8)(surf->texcoords[surf->triangles[j].indices[k]].t * 256);
			}
			
			triangle->type  = 0;
			triangle->color = 255;
			triangle->texture_num = i;
			triangle->flags = 0;
			
			fwrite (triangle, 1, sizeof (struct u3d_triangle), u3d_data);
		}
		
		index_offset += surf->info.num_verts;
	}

	/* clean up data file stuff */
	U_Free (triangle);
	fclose (u3d_data);

	/* write aniv header */
	aniv_hdr->num_frames = md3->header.num_frames;
	aniv_hdr->frame_size = 0;
	for (i = 1; i < 2/*md3->header.num_surfaces*/; i++)
		aniv_hdr->frame_size += 4 * md3->surfs[i].info.num_verts;
		
	fwrite (aniv_hdr, sizeof (struct u3d_aniv_hdr), 1, u3d_aniv);
	U_Free (aniv_hdr);
	
	/* open uc file */
	u3d_script = fopen (uc_filepath, "w");
	
	if (u3d_script == NULL)
	{
		printf ("Cannot create script file '%s'!\n", uc_filepath);
		U_Free (uc_filepath);
		exit (ERR_NOWRITE);
	}
	
	U_Free (uc_filepath);
	
	/* write basic mesh statements */
	aniv_name = U3D_GetAnivFileName ();
	data_name = U3D_GetDataFileName ();
	
	fprintf (u3d_script, CLASS_DECL_STR, mesh_name);
	fprintf (u3d_script, MESH_IMPORT_STR, mesh_name, aniv_name, data_name);
	fprintf (u3d_script, MESH_ORIGIN_STR, mesh_name);
	fprintf (u3d_script, MESH_SEQ_STR, mesh_name, "All", 0, md3->header.num_frames);
	
	U_Free (aniv_name);
	U_Free (data_name);

	/* write aniv data */
	for (i = 0; i < md3->header.num_frames; i++)
	{
		if (frame)
			last_frame_group = MD3_GetAnimGroup (frame->name);
		
		frame = &md3->frames[i];
		
		for (j = 1; j < 2/*md3->header.num_surfaces*/; j++)
		{			
			surf = &md3->surfs[i];
			for (k = 0; k < surf->info.num_verts; k++)
			{
				vtx = &surf->xyz_norms[k];
				
				v[0] = vtx->x * MD3_XYZ_SCALE;
				v[1] = vtx->y * MD3_XYZ_SCALE;
				v[2] = vtx->z * MD3_XYZ_SCALE;
				
				aniv_vertex = ((int)(v[0] * 8.0) & 0x7FF) | (((int)(v[1] * 8.0) & 0x7FF) << 11) | (((int)(v[2] * 4.0) & 0x3FF) << 22);
				fwrite (&aniv_vertex, 4, 1, u3d_aniv);
			}
		}
		
		/* check if current frame name contains the name of the last animation frame */
		if (strstr (frame->name, last_frame_group) == NULL)
		{
			fprintf (u3d_script, MESH_SEQ_STR, mesh_name, last_frame_group, start_frame, i - start_frame);
			start_frame = i;
		}
		else if (md3->header.num_frames == (i+1))
			fprintf (u3d_script, MESH_SEQ_STR, mesh_name, last_frame_group, start_frame, i - start_frame + 1);
		
		U_Free (last_frame_group);
	}
	
	/* write meshmap scale and texture import execs */
	fprintf (u3d_script, TEX_IMPORT_STR, mesh_name, mesh_name); 
	fprintf (u3d_script, MESH_SCALE_STR, mesh_name);
	fprintf (u3d_script, MESH_SETTEX_STR, mesh_name, 0, mesh_name);

	/* final cleanup */
	fclose (u3d_script);
	fclose (u3d_aniv);
	
	return 1;
}

void MD3_Free (struct md3_model *mdl)
{
	int i;
	struct md3_surface *surf;
	
	for (i = 0; i < mdl->header.num_surfaces; i++)
	{
		surf = &mdl->surfs[i];
		U_Free (surf->xyz_norms);
		U_Free (surf->texcoords);
		U_Free (surf->triangles);
		U_Free (surf->shaders);
	}
	
	U_Free (mdl->surfs);
	U_Free (mdl->tags);
	U_Free (mdl->frames);
	U_Free (mdl);
}
