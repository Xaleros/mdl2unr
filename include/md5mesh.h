/*========================================================================*\
|*  This file is part of mdl2unr.                                         *|
|*                                                                        *|
|*  mdl2unr is free software: you can redistribute it and/or modify       *|
|*  it under the terms of the GNU General Public License as published by  *|
|*  the Free Software Foundation, either version 3 of the License, or     *|
|*  (at your option) any later version.                                   *|
|*                                                                        *|
|*  mdl2unr is distributed in the hope that it will be useful,             *|
|*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *|
|*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *|
|*  GNU General Public License for more details.                          *|
|*                                                                        *|
|*  You should have received a copy of the GNU General Public License     *|
|*  along with mdl2unr.  If not, see <http://www.gnu.org/licenses/>.      *|
|*                                                                        *|
\*========================================================================*/

/*========================================================================
 * md5mesh.h : idTech 4 mesh data structures (no animation data)
 * 
 * written by Adam 'Xaleros' Smith
 * info/structs from http://tfc.duke.free.fr/coding/md5-specs-en.html
 *========================================================================
*/

#include "types.h"

struct md5_mesh_joint
{
	char name[64];
	int  parent_idx;
	vec4_t position;
	vec4_t orientation;
};

struct md5_mesh_vertex
{
	
};

struct md5_mesh_group
{
	int num_vertices;
	struct md5_mesh_vertex *vertices;
	
	int num_tris;
	struct md5_mesh_triangle *triangles;
	
	int num_weights;
	struct md5_mesh_weight *weights;
	
	
};

struct md5_mesh
{
	u32 version
	
	u32 num_joints;
	md5_mesh_joint *joints;
	
	u32 num_groups;
	md5_mesh_group *groups;
};

struct md5_model
{
	struct md5_mesh *mesh;
};

int MD5Mesh_Load (const char *filename, struct md5_mesh *mesh);
