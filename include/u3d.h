/*========================================================================*\
|*  This file is part of mdl2unr.                                         *|
|*                                                                        *|
|*  mdl2unr is free software: you can redistribute it and/or modify       *|
|*  it under the terms of the GNU General Public License as published by  *|
|*  the Free Software Foundation, either version 3 of the License, or     *|
|*  (at your option) any later version.                                   *|
|*                                                                        *|
|*  mdl2unr is distributed in the hope that it will be useful,             *|
|*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *|
|*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *|
|*  GNU General Public License for more details.                          *|
|*                                                                        *|
|*  You should have received a copy of the GNU General Public License     *|
|*  along with mdl2unr.  If not, see <http://www.gnu.org/licenses/>.      *|
|*                                                                        *|
\*========================================================================*/

/*========================================================================
 * u3d.h : Unreal vertex mesh/animation structures and functions
 * 
 * written by Adam 'Xaleros' Smith
 * info/structs from http://paulbourke.net/dataformats/unreal/
 *========================================================================
*/

#include "types.h"

struct u3d_data_hdr
{
	u16 num_polygons;
	u16 num_vertices;
	u16 bogus_rot;
	u16 bogus_frame;
	u32 bogus_norm_x;
	u32 bogus_norm_y;
	u32 bogus_norm_z;
	u32 fix_scale;
	u32 unused[3];
	u8  unknown[12];

};

struct u3d_aniv_hdr
{
	u16 num_frames;
	u16 frame_size;
};

struct u3d_triangle
{
	u16  vertex[3];
	char type;
	u8   color;
	u8   tex[3][2];
	char texture_num;
	char flags;
};


