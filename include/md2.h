/*========================================================================*\
|*  This file is part of mdl2unr.                                         *|
|*                                                                        *|
|*  mdl2unr is free software: you can redistribute it and/or modify       *|
|*  it under the terms of the GNU General Public License as published by  *|
|*  the Free Software Foundation, either version 3 of the License, or     *|
|*  (at your option) any later version.                                   *|
|*                                                                        *|
|*  mdl2unr is distributed in the hope that it will be useful,             *|
|*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *|
|*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *|
|*  GNU General Public License for more details.                          *|
|*                                                                        *|
|*  You should have received a copy of the GNU General Public License     *|
|*  along with mdl2unr.  If not, see <http://www.gnu.org/licenses/>.      *|
|*                                                                        *|
\*========================================================================*/

/*========================================================================
 * md2.h : idTech 2 mesh/animation data structures and functions
 * 
 * written by Adam 'Xaleros' Smith
 * info/structs from http://tfc.duke.free.fr/coding/md2-specs-en.html
 *========================================================================
*/

#include "types.h"
#include "state.h"

/* MD2 header */
struct md2_header
{
  int ident;                  /* magic number: "IDP2" */
  int version;                /* version: must be 8 */

  int skinwidth;              /* texture width */
  int skinheight;             /* texture height */

  int framesize;              /* size in bytes of a frame */

  int num_skins;              /* number of skins */
  int num_vertices;           /* number of vertices per frame */
  int num_st;                 /* number of texture coordinates */
  int num_tris;               /* number of triangles */
  int num_glcmds;             /* number of opengl commands */
  int num_frames;             /* number of frames */

  int offset_skins;           /* offset skin data */
  int offset_st;              /* offset texture coordinate data */
  int offset_tris;            /* offset triangle data */
  int offset_frames;          /* offset frame data */
  int offset_glcmds;          /* offset OpenGL command data */
  int offset_end;             /* offset end of file */
};

/* Texture name */
struct md2_skin
{
  char name[64];              /* texture file name */
};

/* Texture coords */
struct md2_texCoord
{
  i16 s;
  i16 t;
};

/* Triangle info */
struct md2_triangle
{
  u16 vertex[3];   /* vertex indices of the triangle */
  u16 st[3];       /* tex. coord. indices */
};

/* Compressed vertex */
struct md2_vertex
{
  u8 v[3];         /* position */
  u8 normalIndex;  /* normal vector index */
};

/* Model frame */
struct md2_frame
{
  vec3_t scale;               /* scale factor */
  vec3_t translate;           /* translation vector */
  char name[16];              /* frame name */
  struct md2_vertex *verts; /* list of frame's vertices */
};

struct md2_model
{
	struct md2_header    header;
	struct md2_skin     *skins;
	struct md2_texCoord *texcoords;
	struct md2_triangle *triangles;
	struct md2_frame    *frames;

};

int  MD2_Load (const char *filename, struct md2_model *mdl);
int  MD2_To_U3D (struct md2_model *mdl);
void MD2_Free (struct md2_model *mdl);
