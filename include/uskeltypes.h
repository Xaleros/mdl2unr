/*========================================================================*\
|*  This file is part of mdl2unr.                                         *|
|*                                                                        *|
|*  mdl2unr is free software: you can redistribute it and/or modify       *|
|*  it under the terms of the GNU General Public License as published by  *|
|*  the Free Software Foundation, either version 3 of the License, or     *|
|*  (at your option) any later version.                                   *|
|*                                                                        *|
|*  mdl2unr is distributed in the hope that it will be useful,             *|
|*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *|
|*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *|
|*  GNU General Public License for more details.                          *|
|*                                                                        *|
|*  You should have received a copy of the GNU General Public License     *|
|*  along with mdl2unr.  If not, see <http://www.gnu.org/licenses/>.      *|
|*                                                                        *|
\*========================================================================*/

/*========================================================================
 * uskeltypes.h : Structs useful for Unreal Engine skeletal meshes
 * 
 * written by Adam 'Xaleros' Smith
 * info/structs from https://wiki.beyondunreal.com/PSK_%26_PSA_file_formats
 *========================================================================
*/

#include "types.h"

struct u_skel_hdr
{
	u8  id[20]; 	  /* Chunk ID */
	int type;       /* Type flags */
	int data_size;  /* Length of following data records */
	int data_count; /* Number of data records */
};

struct u_skel_jointpos
{
	vec4_t orientation;
	vec3_t position;
	float  length;
	float  xsize;
	float  ysize;
	float  zsize;
};

struct u_skel_bone
{
	char name[64];
	int  flags;
	int  num_children;
	int  parent_idx;
	
	struct u_skel_jointpos bone_pos;
};
