/*========================================================================*\
|*  This file is part of mdl2unr.                                         *|
|*                                                                        *|
|*  mdl2unr is free software: you can redistribute it and/or modify       *|
|*  it under the terms of the GNU General Public License as published by  *|
|*  the Free Software Foundation, either version 3 of the License, or     *|
|*  (at your option) any later version.                                   *|
|*                                                                        *|
|*  mdl2unr is distributed in the hope that it will be useful,             *|
|*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *|
|*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *|
|*  GNU General Public License for more details.                          *|
|*                                                                        *|
|*  You should have received a copy of the GNU General Public License     *|
|*  along with mdl2unr.  If not, see <http://www.gnu.org/licenses/>.      *|
|*                                                                        *|
\*========================================================================*/

/*========================================================================
 * error.h : error code definitions
 * 
 * written by Adam 'Xaleros' Smith
 *========================================================================
*/

/* error types */
#define ERR_UNKNOWN -1
#define ERR_OK       0
#define ERR_UNKARG   1
#define ERR_FILEMIS  2
#define ERR_BADIMP   3
#define ERR_UNKIMP   4
#define ERR_NOWRITE  5
#define ERR_NOMEM    6
