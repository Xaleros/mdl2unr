/*========================================================================*\
|*  This file is part of mdl2unr.                                         *|
|*                                                                        *|
|*  mdl2unr is free software: you can redistribute it and/or modify       *|
|*  it under the terms of the GNU General Public License as published by  *|
|*  the Free Software Foundation, either version 3 of the License, or     *|
|*  (at your option) any later version.                                   *|
|*                                                                        *|
|*  mdl2unr is distributed in the hope that it will be useful,             *|
|*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *|
|*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *|
|*  GNU General Public License for more details.                          *|
|*                                                                        *|
|*  You should have received a copy of the GNU General Public License     *|
|*  along with mdl2unr.  If not, see <http://www.gnu.org/licenses/>.      *|
|*                                                                        *|
\*========================================================================*/

/*========================================================================
 * uscript.h : Preprocessor definitions for writing .uc files
 * 
 * written by Adam 'Xaleros' Smith
 *========================================================================
*/

#define CLASS_DECL_STR \
	"class %s extends Actor;\n\n"
	
#define MESH_IMPORT_STR \
	"#exec MESH IMPORT MESH=%s ANIVFILE=Models\\%s DATAFILE=Models\\%s\n"
	
#define MESH_ORIGIN_STR \
	"#exec MESH ORIGIN MESH=%s X=0 Y=0 Z=0 YAW=128\n\n"
	
#define MESH_SEQ_STR \
	"#exec MESH SEQUENCE MESH=%s SEQ=%s STARTFRAME=%i NUMFRAMES=%i\n"
	
#define MESH_SCALE_STR \
	"#exec MESHMAP SCALE MESHMAP=%s X=-0.005 Y=0.005 Z=0.01\n"

#define MESH_SETTEX_STR \
	"#exec MESHMAP SETTEXTURE MESHMAP=%s NUM=%i TEXTURE=%s_skin\n"

#define TEX_IMPORT_STR \
	"\n#exec TEXTURE IMPORT NAME=%s_skin FILE=Models\\%s_skin.pcx GROUP=Skins\n"
