/*========================================================================*\
|*  This file is part of mdl2unr.                                         *|
|*                                                                        *|
|*  mdl2unr is free software: you can redistribute it and/or modify       *|
|*  it under the terms of the GNU General Public License as published by  *|
|*  the Free Software Foundation, either version 3 of the License, or     *|
|*  (at your option) any later version.                                   *|
|*                                                                        *|
|*  mdl2unr is distributed in the hope that it will be useful,             *|
|*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *|
|*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *|
|*  GNU General Public License for more details.                          *|
|*                                                                        *|
|*  You should have received a copy of the GNU General Public License     *|
|*  along with mdl2unr.  If not, see <http://www.gnu.org/licenses/>.      *|
|*                                                                        *|
\*========================================================================*/

/*========================================================================
 * psa.h : Unreal Engine skeletal mesh animation structures
 * 
 * written by Adam 'Xaleros' Smith
 * info/structs from https://wiki.beyondunreal.com/PSK_%26_PSA_file_formats
 *========================================================================
*/

#include "uskeltypes.h"

struct u_psa_animinfo
{
	char  name[64];
	char  group[64];
	int   total_bones;
	int   root_include;
	int   key_compression_style;
	int   key_quotum;
	float key_reduction;
	float track_time;
	float anim_rate;
	int   start_bone;
	int   first_raw_frame;
	int   num_raw_frames;	
};

struct u_psa_qanimkey
{
	vec3_t position;
	vec4_t orientation;
	float  time;
};

struct u_psa
{
	struct u_skel_hdr header;
	
	struct u_skel_hdr   bones_hdr;
	struct u_skel_bone *bones;
	
	struct u_skel_hdr      anim_infos_hdr;
	struct u_psa_animinfo *anim_infos;
	
	struct u_skel_hdr      anim_keys_hdr;
	struct u_psa_qanimkey *anim_keys;
};
