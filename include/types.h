/*========================================================================*\
|*  This file is part of mdl2unr.                                         *|
|*                                                                        *|
|*  mdl2unr is free software: you can redistribute it and/or modify       *|
|*  it under the terms of the GNU General Public License as published by  *|
|*  the Free Software Foundation, either version 3 of the License, or     *|
|*  (at your option) any later version.                                   *|
|*                                                                        *|
|*  mdl2unr is distributed in the hope that it will be useful,             *|
|*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *|
|*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *|
|*  GNU General Public License for more details.                          *|
|*                                                                        *|
|*  You should have received a copy of the GNU General Public License     *|
|*  along with mdl2unr.  If not, see <http://www.gnu.org/licenses/>.      *|
|*                                                                        *|
\*========================================================================*/

/*========================================================================
 * types.h : Useful data typedefs and preprocessor definitions
 * 
 * written by Adam 'Xaleros' Smith
 *========================================================================
*/

#include "config.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#ifndef M2U_TYPES_H
#define M2U_TYPES_H

#define TYPE_IMPORT 1
#define TYPE_EXPORT 2

/* import types */
#define IMP_UNK 0
#define IMP_MDL 1
#define IMP_MD2 2
#define IMP_MD3 3
#define IMP_MD4 4
#define IMP_MD5 5

/* some windows stuff */
#ifdef _WIN32
	#define __STDC_FORMAT_MACRO 1
	#include <inttypes.h>
	#ifdef _WIN64
		#define PRSizeT PRIu64
	#else
		#define PRSizeT PRIu32
	#endif
#else
	#define PRSizeT "zu"
#endif

/* short hand data types */
typedef unsigned char  u8;
typedef unsigned short u16;
typedef unsigned int   u32;
typedef unsigned long long u64;

typedef signed char  i8;
typedef signed short i16;
typedef signed int   i32;
typedef signed long long i64;

/* Vector */
typedef float vec3_t[3];

/* Quaternion */
typedef float vec4_t[4];

#endif
