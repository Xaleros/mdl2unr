/*========================================================================*\
|*  This file is part of mdl2unr.                                         *|
|*                                                                        *|
|*  mdl2unr is free software: you can redistribute it and/or modify       *|
|*  it under the terms of the GNU General Public License as published by  *|
|*  the Free Software Foundation, either version 3 of the License, or     *|
|*  (at your option) any later version.                                   *|
|*                                                                        *|
|*  mdl2unr is distributed in the hope that it will be useful,             *|
|*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *|
|*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *|
|*  GNU General Public License for more details.                          *|
|*                                                                        *|
|*  You should have received a copy of the GNU General Public License     *|
|*  along with mdl2unr.  If not, see <http://www.gnu.org/licenses/>.      *|
|*                                                                        *|
\*========================================================================*/

/*========================================================================
 * state.h : Variables and functions useful to the state of the program
 * 
 * written by Adam 'Xaleros' Smith
 *========================================================================
*/

#include "types.h"

int import_format;

extern char *mdl_filename;
extern char *u3d_filename;

char *U3D_GetDataFilePath ();
char *U3D_GetAnivFilePath ();

char *U3D_GetDataFileName ();
char *U3D_GetAnivFileName ();

char *U3D_GetScriptFilePath ();
const char *U3D_GetMeshMapName ();

int Sys_GetFormatFromFilename (const char *filename);
void Sys_ReplaceChar (char *str, char old_char, char new_char);
