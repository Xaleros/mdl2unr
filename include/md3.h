/*========================================================================*\
|*  This file is part of mdl2unr.                                         *|
|*                                                                        *|
|*  mdl2unr is free software: you can redistribute it and/or modify       *|
|*  it under the terms of the GNU General Public License as published by  *|
|*  the Free Software Foundation, either version 3 of the License, or     *|
|*  (at your option) any later version.                                   *|
|*                                                                        *|
|*  mdl2unr is distributed in the hope that it will be useful,             *|
|*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *|
|*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *|
|*  GNU General Public License for more details.                          *|
|*                                                                        *|
|*  You should have received a copy of the GNU General Public License     *|
|*  along with mdl2unr.  If not, see <http://www.gnu.org/licenses/>.      *|
|*                                                                        *|
\*========================================================================*/

/*========================================================================
 * md3.h : routines for converting idTech 3 meshes to Unreal Engine 1
 * 
 * written by Adam 'Xaleros' Smith
 * info/structs from https://icculus.org/homepages/phaethon/q3a/formats/md3format.html
 *========================================================================
*/

#include "types.h"

/* MD3->IDENT listed at the above url is INCORRECT */
/* this is the correct one */
#define MD3_IDENT 0x33504449

struct md3_header
{
	int  ident;
	int  version;
	char name[64];
	int  flags;
	int  num_frames;
	int  num_tags;
	int  num_surfaces;
	int  num_skins;
	int  offset_frames;
	int  offset_tags;
	int  offset_surfaces;
	int  offset_eof;
};

struct md3_frame
{
	vec3_t min_bounds;
	vec3_t max_bounds;
	vec3_t local_origin;
	float  radius;
	char   name[16];
};

struct md3_tag
{
	char name[64];
	vec3_t origin;
	vec3_t axis[3];
};

struct md3_surf_info
{
	int  ident;
	char name[64];
	int  flags;
	int  num_frames;
	int  num_shaders;
	int  num_verts;
	int  num_tris;
	int  offset_tris;
	int  offset_shaders;
	int  offset_st;
	int  offset_xyznorm;
	int  offset_end;
};

struct md3_surface
{
	struct md3_surf_info info;
	struct md3_shader   *shaders;
	struct md3_triangle *triangles;
	struct md3_texCoord *texcoords;
	struct md3_vertex   *xyz_norms;
};

struct md3_shader
{
	char name[64];
	int  index;
};

struct md3_triangle
{
	int indices[3];
};

struct md3_texCoord
{
	float s;
	float t;
};

struct md3_vertex
{
	u16 x;
	u16 y;
	u16 z;
	u16 normal;
};

struct md3_model
{
	struct md3_header   header;
	struct md3_frame   *frames;
	struct md3_tag     *tags;
	struct md3_surface *surfs;
};

int  MD3_Load (const char *filename, struct md3_model *mdl);
int  MD3_To_U3D (struct md3_model *mdl);
void MD3_Free (struct md3_model *mdl);
