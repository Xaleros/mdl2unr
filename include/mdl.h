/*========================================================================*\
|*  This file is part of mdl2unr.                                         *|
|*                                                                        *|
|*  mdl2unr is free software: you can redistribute it and/or modify       *|
|*  it under the terms of the GNU General Public License as published by  *|
|*  the Free Software Foundation, either version 3 of the License, or     *|
|*  (at your option) any later version.                                   *|
|*                                                                        *|
|*  mdl2unr is distributed in the hope that it will be useful,             *|
|*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *|
|*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *|
|*  GNU General Public License for more details.                          *|
|*                                                                        *|
|*  You should have received a copy of the GNU General Public License     *|
|*  along with mdl2unr.  If not, see <http://www.gnu.org/licenses/>.      *|
|*                                                                        *|
\*========================================================================*/

/*========================================================================
 * mdl.h : idTech 1 mesh/animation data structures and functions
 * 
 * written by Adam 'Xaleros' Smith
 * info/structs from http://tfc.duke.free.fr/coding/mdl-specs-en.html
 *========================================================================
*/

#include "types.h"
#include "state.h"

/* MDL header */
struct mdl_header
{
  int ident;            /* magic number: "IDPO" */
  int version;          /* version: 6 */

  vec3_t scale;         /* scale factor */
  vec3_t translate;     /* translation vector */
  float boundingradius;
  vec3_t eyeposition;   /* eyes' position */

  int num_skins;        /* number of textures */
  int skinwidth;        /* texture width */
  int skinheight;       /* texture height */

  int num_verts;        /* number of vertices */
  int num_tris;         /* number of triangles */
  int num_frames;       /* number of frames */

  int synctype;         /* 0 = synchron, 1 = random */
  int flags;            /* state flag */
  float size;
};

/* Skin */
struct mdl_skin
{
  int group;      /* 0 = single, 1 = group */
  GLubyte *data;  /* texture data */
};


/* Group of pictures */
struct mdl_groupskin
{
    int group;     /* 1 = group */
    int nb;        /* number of pics */
    float *time;   /* time duration for each pic */
    ubyte **data;  /* texture data */
};

/* Texture coords */
struct mdl_texcoord
{
  int onseam;
  int s;
  int t;
};

/* Triangle info */
struct mdl_triangle
{
  int facesfront;  /* 0 = backface, 1 = frontface */
  int vertex[3];   /* vertex indices */
};


/* Compressed vertex */
struct mdl_vertex
{
  unsigned char v[3];
  unsigned char normalIndex;
};

/* Simple frame */
struct mdl_simpleframe
{
  struct mdl_vertex bboxmin; /* bouding box min */
  struct mdl_vertex boxmax; /* bouding box max */
  char name[16];
  struct mdl_vertex *verts;  /* vertex list of the frame */
};

/* Model frame */
struct mdl_frame
{
  int type;                        /* 0 = simple, !0 = group */
  struct mdl_simpleframe frame;   /* this program can't read models
				                             composed of group frames! */
};

/* Group of simple frames */
struct mdl_groupframe
{
  int type;                         /* !0 = group */
  struct mdl_vertex min;          /* min pos in all simple frames */
  struct mdl_vertex max;          /* max pos in all simple frames */
  float *time;                      /* time duration for each frame */
  struct mdl_simpleframe *frames; /* simple frame list */
};

struct mdl_model
{
	mdl_header header;
	mdl_skin  *skins;
	
};

int  MDL_Load (const char *filename, struct mdl_model *mdl);
int  MDL_To_U3D (struct mdl_model *mdl);
void MDL_Free (struct mdl_model *mdl);

