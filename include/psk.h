/*========================================================================*\
|*  This file is part of mdl2unr.                                         *|
|*                                                                        *|
|*  mdl2unr is free software: you can redistribute it and/or modify       *|
|*  it under the terms of the GNU General Public License as published by  *|
|*  the Free Software Foundation, either version 3 of the License, or     *|
|*  (at your option) any later version.                                   *|
|*                                                                        *|
|*  mdl2unr is distributed in the hope that it will be useful,             *|
|*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *|
|*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *|
|*  GNU General Public License for more details.                          *|
|*                                                                        *|
|*  You should have received a copy of the GNU General Public License     *|
|*  along with mdl2unr.  If not, see <http://www.gnu.org/licenses/>.      *|
|*                                                                        *|
\*========================================================================*/

/*========================================================================
 * psk.h : Unreal Engine skeletal mesh geometry/data structures
 * 
 * written by Adam 'Xaleros' Smith
 * info/structs from https://wiki.beyondunreal.com/PSK_%26_PSA_file_formats
 *========================================================================
*/

#include "uskeltypes.h"

struct u_psk_point
{
	vec3_t point;
};

struct u_psk_vertex
{
	u16   point_idx; /* <-- uhm? this data arrangement is not very optimal */
	float u;           /* <-- watch how values are assigned */
	float v;
	u8    mat_idx;   /* potentially unused */
	u8    _pad0;
};

struct u_psk_triangle
{
	u16 wedge_idx[3];
	u8  mat_idx;
	u8  aux_mat_idx;
	int smoothing_groups;
};

struct u_psk_mat
{
	char mat_name[64];
	int  texture_idx;
	int  poly_flags;
	int  aux_material;
	int  aux_flags;
	int  lod_bias;
	int  lod_style;
};

struct u_psk_rbinf
{
	float weight;
	int   point_idx;
	int   bone_idx;
};

struct u_psk
{
	struct u_skel_hdr header;
	
	struct u_skel_hdr   points_hdr;
	struct u_psk_point *points;
	
	struct u_skel_hdr    wedges_hdr;
	struct u_psk_vertex *wedges;
	
	struct u_skel_hdr      faces_hdr;
	struct u_psk_triangle *faces
	
	struct u_skel_hdr materials_hdr;
	struct u_psk_mat *materials;
	
	struct u_skel_hdr   ref_bones_hdr;
	struct u_skel_bone *ref_bones;
	
	struct u_skel_hdr   influences_hdr;
	struct u_psk_rbinf *influences;
};
