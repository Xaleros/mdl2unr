/*========================================================================*\
|*  This file is part of mdl2unr.                                         *|
|*                                                                        *|
|*  mdl2unr is free software: you can redistribute it and/or modify       *|
|*  it under the terms of the GNU General Public License as published by  *|
|*  the Free Software Foundation, either version 3 of the License, or     *|
|*  (at your option) any later version.                                   *|
|*                                                                        *|
|*  mdl2unr is distributed in the hope that it will be useful,             *|
|*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *|
|*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *|
|*  GNU General Public License for more details.                          *|
|*                                                                        *|
|*  You should have received a copy of the GNU General Public License     *|
|*  along with mdl2unr.  If not, see <http://www.gnu.org/licenses/>.      *|
|*                                                                        *|
\*========================================================================*/

/*========================================================================
 * mem.h : Preprocessor declarations for various memory allocators and
 *         helper function definitions
 * 
 * written by Adam 'Xaleros' Smith
 *========================================================================
*/

#include <stdlib.h>
#include <stdio.h>

#if defined HAVE_JEMALLOC
  #include <jemalloc/jemalloc.h>
	#define _m2u_malloc  malloc
	#define _m2u_realloc realloc
	#define U_Free       free
#else
	#define _m2u_malloc  malloc
	#define _m2u_realloc realloc
	#define U_Free       free
#endif

void *U_Malloc  (size_t size);
void *U_Realloc (void *ptr, size_t size);
